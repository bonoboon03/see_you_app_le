class StockResponse {
  String productName;
  int stockQuantity;
  int minQuantity;

  StockResponse(this.productName, this.stockQuantity, this.minQuantity);

  factory StockResponse.fromJson(Map<String, dynamic> json) {
    return StockResponse(
      json['productName'],
      json['stockQuantity'],
      json['minQuantity'],
    );
  }
}