class BoardListDetailItem {
  String content;
  String title;
  String dateWrite;

  BoardListDetailItem(this.content, this.title, this.dateWrite);

  factory BoardListDetailItem.fromJson(Map<String, dynamic> json) {
    return BoardListDetailItem(
      json['content'],
      json['title'],
      json['dateWrite'],
    );
  }
}