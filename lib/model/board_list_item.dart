class BoardListItem {
  int id;
  String title;
  String dateWrite;

  BoardListItem(this.id, this.title, this.dateWrite);

  factory BoardListItem.fromJson(Map<String, dynamic> json) {
    return BoardListItem(
      json['id'],
      json['title'],
      json['dateWrite'],
    );
  }
}