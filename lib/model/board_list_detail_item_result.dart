import 'package:see_you_app/model/board_list_detail_item.dart';

class BoardListDetailItemResult {
  int code;
  bool isSuccess;
  String msg;
  BoardListDetailItem data;

  BoardListDetailItemResult(this.code, this.isSuccess, this.msg, this.data);

  factory BoardListDetailItemResult.fromJson(Map<String, dynamic> json) {
    return BoardListDetailItemResult(
      json['code'],
      json['isSuccess'],
      json['msg'],
      BoardListDetailItem.fromJson(json['data']),
    );
  }
}