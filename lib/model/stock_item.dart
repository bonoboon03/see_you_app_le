class StockItem {
  int id;
  String productName;
  int stockQuantity;
  int minQuantity;

  StockItem(this.id, this.productName, this.stockQuantity, this.minQuantity);

  factory StockItem.fromJson(Map<String, dynamic> json) {
    return StockItem(
      json['id'],
      json['productName'],
      json['stockQuantity'],
      json['minQuantity']
    );
  }
}