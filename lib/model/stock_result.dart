import 'package:see_you_app/model/stock_response.dart';

class StockResult {
  StockResponse data;
  bool isSuccess; // 성공 여부를 묻는 변수
  int code; // isSuccess 결과에 따른 코드 값을 담는 변수
  String msg; // isSuccess 결과에 따른 메세지를 담는 변수

  StockResult(this.data, this.isSuccess, this.code, this.msg);

  factory StockResult.fromJson(Map<String, dynamic> json) {
    return StockResult(
        StockResponse.fromJson(json['data']),
        json['isSuccess'] as bool,
        json['code'],
        json['msg']
    );
  }
}