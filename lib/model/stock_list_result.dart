import 'package:see_you_app/model/stock_item.dart';

class StockListResult {
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<StockItem>? list;

  StockListResult(this.totalItemCount, this.totalPage, this.currentPage, {this.list});

  factory StockListResult.fromJson(Map<String, dynamic> json) {
    return StockListResult(
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      list: json['list'] != null ? (json['list'] as List).map((e) => StockItem.fromJson(e)).toList() : [],
    );
  }
}