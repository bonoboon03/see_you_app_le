import 'package:flutter/material.dart';
import 'package:see_you_app/functions/token_lib.dart';
import 'package:see_you_app/pages/page_login.dart';
import 'package:see_you_app/pages/page_main.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    String? memberToken = await TokenLib.getMemberToken();

    if (memberToken == null) {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin()), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageMain()), (route) => false);
    }
  }
}
