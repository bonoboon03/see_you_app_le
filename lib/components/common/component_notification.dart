import 'package:flutter/material.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:see_you_app/config/config_color.dart';

/*
알림창 띄우는 컴포넌트.
!! 복사해서 쓰는 코드!!
 */
class ComponentNotification {
  final bool success;
  final String title;
  final String subTitle;

  ComponentNotification({required this.success, required this.title, required this.subTitle});

  // 성공했을 때 컬러와 실패했을 때 컬러 구분
  Color _getColor() {
    if (success) {
      return colorPrimary;
    } else {
      return colorRed;
    }
  }

  // 성공했을 때 아이콘과 실패했을 때 아이콘 구분
  IconData _getIcon() {
    if (success) {
      return Icons.check;
    } else {
      return Icons.clear;
    }
  }

  void call() {
    BotToast.showNotification(
      align: const Alignment(0, 0.99),
      leading: (cancel) => SizedBox.fromSize(
        size: const Size(40, 40),
        child: IconButton(
          icon: Icon(_getIcon(), color: _getColor()),
          onPressed: cancel,
        ),
      ),
      title: (_) => Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
              color: _getColor(),
            ),
            textAlign: TextAlign.start,
          ),
          const SizedBox(height: 15,)
        ],
      ),
      subtitle: (_) => Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            subTitle,
            style: const TextStyle(
              color: Colors.black,
              height: 1.3,
            ),
            textAlign: TextAlign.start,
          )
        ],
      ),
      contentPadding: const EdgeInsets.all(8),
      duration: const Duration(seconds: 3),
      borderRadius: 0.0,
    );
  }
}
