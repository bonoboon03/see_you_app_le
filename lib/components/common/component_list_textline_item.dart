import 'package:flutter/material.dart';
import 'package:see_you_app/components/common/component_dot.dart';
import 'package:see_you_app/components/common/component_margin_vertical.dart';
import 'package:see_you_app/config/config_color.dart';
import 'package:see_you_app/config/config_size.dart';
import 'package:see_you_app/config/config_style.dart';
import 'package:see_you_app/enums/enum_size.dart';

class ComponentListTextLineItem extends StatelessWidget {
  const ComponentListTextLineItem({
    super.key,
    required this.title, // 얘네는 필수
    required this.voidCallback, // 얘네는 필수
    this.isUseContent1Line = false, // 언급안하면 기본값 아니오
    this.content1Subject = "", // 언급안하면 기본값 빈스트링
    this.content1Text = "", // 언급안하면 기본값 빈스트링
    this.isUseContent2Line = false, // 언급안하면 기본값 아니오
    this.content2Subject = "", // 언급안하면 기본값 빈스트링
    this.content2Text = "", // 언급안하면 기본값 빈스트링
    this.isUseContent3Line = false, // 언급안하면 기본값 아니오
    this.content3Subject = "", // 언급안하면 기본값 빈스트링
    this.content3Text = "", // 언급안하면 기본값 빈스트링
  });

  final String title; // 제목
  final bool isUseContent1Line; // 첫번째줄 사용하겠냐?
  final String content1Subject; // 첫번째줄 항목명
  final String content1Text; // 첫번째줄 내용
  final bool isUseContent2Line; // 두번째줄 사용하겠냐?
  final String content2Subject; // 두번째줄 항목명
  final String content2Text; // 두번째줄 내용
  final bool isUseContent3Line; // 세번째줄 사용하겠냐?
  final String content3Subject; // 세번째줄 항목명
  final String content3Text; // 세번째줄 내용
  final VoidCallback voidCallback; // 야 클릭됐다~~ 니가 처리해~~ 신호주기

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: voidCallback,
      child: Container(
        padding: const EdgeInsets.all(15),
        margin: const EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: colorLightGray),
          borderRadius: BorderRadius.circular(buttonRadius),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              title,
              style: const TextStyle(
                fontSize: fontSizeBig,
                fontWeight: FontWeight.w700,
                wordSpacing: 1.5,
                height: 1.3,
              ),
            ),
            const ComponentMarginVertical(),
            isUseContent1Line ? Row(
              children: [
                Text(content1Subject),
                const ComponentDot(),
                Text(content1Text),
              ],
            ) : Container(),
            const ComponentMarginVertical(enumSize: EnumSize.micro),
            isUseContent2Line ? Row(
              children: [
                Text(content2Subject),
                const ComponentDot(),
                Text(content2Text),
              ],
            ) : Container(),
            const ComponentMarginVertical(enumSize: EnumSize.micro),
            isUseContent3Line ? Row(
              children: [
                Text(content3Subject),
                const ComponentDot(),
                Text(content3Text),
              ],
            ) : Container(),
          ],
        ),
      ),
    );
  }
}