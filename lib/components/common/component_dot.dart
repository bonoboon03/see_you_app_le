import 'package:flutter/material.dart';
import 'package:see_you_app/config/config_color.dart';

class ComponentDot extends StatelessWidget {
  const ComponentDot({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(3, 0, 3, 0),
      child: const Icon(
        Icons.stop,
        size: 7,
        color: colorGray,
      ),
    );
  }
}
