import 'package:flutter/material.dart';
import 'package:see_you_app/config/config_color.dart';
import 'package:see_you_app/config/config_size.dart';
import 'package:see_you_app/config/config_style.dart';

class ComponentTextBtn extends StatelessWidget {
  final String text;
  final Color bgColor;
  final Color textColor;
  final Color borderColor;
  final VoidCallback callback;
  final IconData? iconData;
  final double? iconSize;

  const ComponentTextBtn(
      {Key? key,
      required this.text,
      required this.callback,
      this.bgColor = colorPrimary,
      this.textColor = Colors.white,
      this.borderColor = colorPrimary,
      this.iconData,
      this.iconSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (iconData != null) {
      return ElevatedButton.icon(
        onPressed: callback,
        icon: Icon(iconData, size: iconSize),
        label: Text(text),
        style: ElevatedButton.styleFrom(
            primary: bgColor,
            onPrimary: textColor,
            padding: contentPaddingButton,
            elevation: buttonElevation,
            textStyle: const TextStyle(
              fontSize: fontSizeMid,
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(buttonRadius),
                side: BorderSide(
                  color: borderColor,
                  width: buttonSideWidth,
                ))),
      );
    } else {
      return ElevatedButton(
        onPressed: callback,
        child: Text(text),
        style: ElevatedButton.styleFrom(
            primary: bgColor,
            onPrimary: textColor,
            padding: contentPaddingButton,
            elevation: buttonElevation,
            textStyle: const TextStyle(
              fontSize: fontSizeMid,
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(buttonRadius),
                side: BorderSide(
                  color: borderColor,
                  width: buttonSideWidth,
                ))),
      );
    }
  }
}
