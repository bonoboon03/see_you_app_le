import 'package:flutter/material.dart';
import 'package:see_you_app/components/common/component_appbar_popup.dart';
import 'package:see_you_app/components/common/component_margin_vertical.dart';
import 'package:see_you_app/components/common/component_text_btn.dart';
import 'package:see_you_app/config/config_color.dart';
import 'package:see_you_app/config/config_size.dart';
import 'package:see_you_app/enums/enum_size.dart';
import 'package:see_you_app/model/stock_result.dart';
import 'package:see_you_app/pages/page_stock_update.dart';
import 'package:see_you_app/repository/repo_stock.dart';

class PageStockListDetail extends StatefulWidget {
  const PageStockListDetail({super.key, required this.id, required this.title});

  final int id;
  final String title;

  @override
  State<PageStockListDetail> createState() => _PageStockListDetailState();
}

class _PageStockListDetailState extends State<PageStockListDetail> {
  String productName = "";
  int stockQuantity = 0;
  int minQuantity = 0;

  Future<void> _getStock() async {
    StockResult result = await RepoStock().getStock(widget.id);

    setState(() {
      productName = result.data.productName;
      stockQuantity = result.data.stockQuantity;
      minQuantity = result.data.minQuantity;
    });
  }

  @override
  void initState() {
    super.initState();
    _getStock();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: widget.title,
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        Container(
          padding: const EdgeInsets.all(20),
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: colorLightGray),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "상품명",
                    style: TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                  Text(
                    productName,
                    style: const TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                ],
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "재고수량",
                    style: TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                  Text(
                    "$stockQuantity",
                    style: const TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                ],
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "최소기준수량",
                    style: TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                  Text(
                    "$minQuantity",
                    style: const TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                ],
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
            ],
          ),
        ),
        const ComponentMarginVertical(),
        Container(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: ComponentTextBtn(
              text: "수정",
              callback: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        PageStockUpdate(id: widget.id)));
          }),
        ),
      ],
    );
  }
}
