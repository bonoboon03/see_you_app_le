import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:see_you_app/components/common/component_appbar_actions.dart';
import 'package:see_you_app/components/common/component_custom_loading.dart';
import 'package:see_you_app/components/common/component_margin_vertical.dart';
import 'package:see_you_app/components/common/component_notification.dart';
import 'package:see_you_app/components/common/component_text_btn.dart';
import 'package:see_you_app/config/config_form_validator.dart';
import 'package:see_you_app/config/config_style.dart';
import 'package:see_you_app/model/update_password_request.dart';
import 'package:see_you_app/pages/page_main.dart';
import 'package:see_you_app/repository/repo_member.dart';
import 'package:see_you_app/styles/style_form_decoration.dart';

class PagePasswordUpdate extends StatefulWidget {
  const PagePasswordUpdate({super.key});

  @override
  State<PagePasswordUpdate> createState() => _PagePasswordUpdateState();
}

class _PagePasswordUpdateState extends State<PagePasswordUpdate> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _updatePassword(UpdatePasswordRequest updatePasswordRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().updatePassword(updatePasswordRequest).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '변경 완료',
        subTitle: '비밀번호 변경이 완료 되었습니다.',
      ).call();

      Navigator.push(
        context, MaterialPageRoute(
          builder: (BuildContext context) => const PageMain()
        )
      );
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '변경 실패',
        subTitle: '비밀번호를 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: "비밀번호 변경",
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        Container(
          margin: const EdgeInsets.all(20),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled, // 자동 유효성 검사 비활성화
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'changePassword',
                    obscureText: true,
                    decoration: StyleFormDecoration().getInputDecoration('새로운 비밀번호'),
                    maxLength: 20,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'changePasswordRe',
                    obscureText: true,
                    decoration: StyleFormDecoration().getInputDecoration('새로운 비밀번호 확인'),
                    maxLength: 20,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'currentPassword',
                    obscureText: true,
                    decoration: StyleFormDecoration().getInputDecoration('현재 비밀번호'),
                    maxLength: 20,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  child: ComponentTextBtn(
                      text : '수정',
                          callback : () {
                    if(_formKey.currentState!.saveAndValidate()) {
                      UpdatePasswordRequest updatePasswordRequest = UpdatePasswordRequest( // LoginRequest에 입력받은 아이디, 비밀번호를 넘겨준다
                        _formKey.currentState!.fields['changePassword']!.value,
                        _formKey.currentState!.fields['changePasswordRe']!.value,
                        _formKey.currentState!.fields['currentPassword']!.value,
                      );
                      _updatePassword(updatePasswordRequest);
                    }
                  }),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}