import 'package:flutter/material.dart';
import 'package:see_you_app/components/common/component_appbar_actions.dart';
import 'package:see_you_app/components/common/component_count_title.dart';
import 'package:see_you_app/components/common/component_list_textline_item.dart';
import 'package:see_you_app/pages/page_money_history_list_detail.dart';

class PageMoneyHistoryList extends StatefulWidget {
  const PageMoneyHistoryList({super.key});

  @override
  State<PageMoneyHistoryList> createState() => _PageMoneyHistoryListState();
}

class _PageMoneyHistoryListState extends State<PageMoneyHistoryList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: "급여 지급 내역 리스트",
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    // if (_totalItemCount > 0) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const ComponentCountTitle(Icons.attachment, 100, '건', '재고'),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: 100,
            itemBuilder: (_, index) =>
              ComponentListTextLineItem(
                title: '직원명',
                isUseContent1Line: true,
                content1Subject: '지급년월',
                content1Text: '2023-10',
                isUseContent2Line: true,
                content2Subject: '급여',
                content2Text: '1,800,000',
                voidCallback: () {
                  Navigator.push(context, MaterialPageRoute(
                      builder: (BuildContext context) => PageMoneyHistoryListDetail()));
                },
              ),
          )
        ],
      ),
    );
    // } else {
    //   // 이용내역이 없을때 흰화면 보이면 에러난것처럼 보이니까 이용내역 없다고 보여주기.
    //   return SizedBox(
    //     height: MediaQuery.of(context).size.height - 30 - 50 - 50 - 70,
    //     child: const ComponentNoContents(
    //       icon: Icons.history,
    //       msg: '재고가 없습니다.',
    //     ),
    //   );
    // }
  }
}
