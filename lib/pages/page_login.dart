import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:see_you_app/components/common/component_appbar_actions.dart';
import 'package:see_you_app/components/common/component_custom_loading.dart';
import 'package:see_you_app/components/common/component_margin_vertical.dart';
import 'package:see_you_app/components/common/component_notification.dart';
import 'package:see_you_app/components/common/component_section_title.dart';
import 'package:see_you_app/components/common/component_text_btn.dart';
import 'package:see_you_app/config/config_form_validator.dart';
import 'package:see_you_app/config/config_style.dart';
import 'package:see_you_app/functions/token_lib.dart';
import 'package:see_you_app/middleware/middleware_login_check.dart';
import 'package:see_you_app/model/login_request.dart';
import 'package:see_you_app/repository/repo_member.dart';
import 'package:see_you_app/styles/style_form_decoration.dart';


class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(String memberGroup, LoginRequest loginRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().doLogin(memberGroup, loginRequest).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '로그인 성공',
        subTitle: '로그인에 성공하였습니다.',
      ).call();

      // api에서 받아온 결과값을 token에 넣는다.
      TokenLib.setMemberToken(res.data.token);
      TokenLib.setMemberName(res.data.name);

      // 미들웨어에게 부탁해서 토큰값 여부 검사 후 페이지 이동을 부탁한다.
      MiddlewareLoginCheck().check(context);

    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '로그인 실패',
        subTitle: '아이디 혹은 비밀번호를 확인해주세요.',
      ).call();
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: '로그인',
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        Stack( // Container가 겹칠 수 있음
          children: [
            Container(
              width: MediaQuery.of(context).size.width, // 사진 꽉 차게
              height: 200,
              color: Colors.white,
              // child: Image.asset('assets/login_img.png', fit: BoxFit.cover), // 비율이 변경되지 않고 꽉 채움. 이미지 잘릴 수 있음.
            ),
          ],
        ),
        const ComponentSectionTitle('로그인이 필요합니다.'),
        Container(
          padding: const EdgeInsets.only(
            bottom: 10,
            left: 20,
            right: 20,
          ),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled, // 자동 유효성 검사 비활성화
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderDropdown<String>(
                    name: 'memberGroup',
                    decoration: InputDecoration(
                      labelText: '회원 그룹',
                    ),
                    onChanged: (value) {
                      setState(() {});
                    },
                    items: const [
                      DropdownMenuItem(value: "manager", child: Text('매니저')),
                      DropdownMenuItem(value: "employee", child: Text('사원')),
                      DropdownMenuItem(value: "part-time", child: Text('아르바이트')),
                    ],
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'username',
                    decoration: StyleFormDecoration().getInputDecoration('아이디'),
                    maxLength: 20,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(5, errorText: formErrorMinLength(5)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    obscureText: true, // 비밀번호 암호화 처리
                    name: 'password',
                    decoration: StyleFormDecoration().getInputDecoration('비밀번호'),
                    maxLength: 20,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: bodyPaddingLeftRight,
          child: ComponentTextBtn(
              text : '로그인',
              callback : () {
            if (_formKey.currentState!.saveAndValidate()) {
              LoginRequest loginRequest = LoginRequest( // LoginRequest에 입력받은 아이디, 비밀번호를 넘겨준다
                  _formKey.currentState!.fields['username']!.value,
                  _formKey.currentState!.fields['password']!.value,
              );
              String memberGroup = _formKey.currentState!.fields['memberGroup']!.value;
              _doLogin(memberGroup, loginRequest);
            }
          }),
        ),
      ],
    );
  }
}
