// import 'package:bot_toast/bot_toast.dart';
// import 'package:flutter/material.dart';
// import 'package:see_you_app/components/common/component_appbar_popup.dart';
// import 'package:see_you_app/components/common/component_custom_loading.dart';
// import 'package:see_you_app/components/common/component_margin_vertical.dart';
// import 'package:see_you_app/components/common/component_notification.dart';
// import 'package:see_you_app/config/config_size.dart';
// import 'package:see_you_app/model/kick_board_start_request.dart';
// import 'package:see_you_apppages/kikboardUsing/page_using_kick_board.dart';
// import 'package:see_you_app/repository/repo_kickboard.dart';
// import 'package:qr_code_dart_scan/qr_code_dart_scan.dart';
//
// class PageQrReader extends StatefulWidget {
//   const PageQrReader({super.key, required this.posX, required this.posY});
//
//   final double posX;
//   final double posY;
//
//   @override
//   State<PageQrReader> createState() => _PageQrReaderState();
// }
//
// class _PageQrReaderState extends State<PageQrReader> {
//   Result? currentResult;
//   QRCodeDartScanController qrCodeDartScanController =
//       QRCodeDartScanController();
//
//   Future<void> _setStart(int kickboardId, KickBoardStartRequest request) async {
//     BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
//       return ComponentCustomLoading(cancelFunc: cancelFunc);
//     });
//
//     await RepoKickboard().setStart(kickboardId, request).then((res) {
//       BotToast.closeAllLoading();
//
//       ComponentNotification(
//         success: true,
//         title: '사용 시작 완료',
//         subTitle: '킥보드 사용이 시작 되었습니다.',
//       ).call();
//
//       Navigator.pushAndRemoveUntil(
//           context,
//           MaterialPageRoute(
//               builder: (BuildContext context) => const PageUsingKickBoard()),
//           (route) => false);
//     }).catchError((err) {
//       BotToast.closeAllLoading();
//
//       ComponentNotification(
//         success: false,
//         title: '사용 시작 실패',
//         subTitle: '킥보드 사용이 실패 했습니다.',
//       ).call();
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: const ComponentAppbarPopup(title: '킥보드 QR코드 스캔'),
//       body: _buildBody(),
//     );
//   }
//
//   //  if (currentResult?.text != null) {
//   //       return Container(
//   //         padding: bodyPaddingLeftRight,
//   //         child: ComponentTextBtn(
//   //           text: '사용 시작',
//   //           callback: () {
//   //             //Navigator.pop(context, int.parse(currentResult!.text));
//   //           },
//   //         ),
//   //       );
//   //     } else {
//   //     }
//   //
//
//   Widget _buildBody() {
//     return QRCodeDartScanView(
//       controller: qrCodeDartScanController,
//       scanInvertedQRCode: true,
//       onCapture: (Result result) {
//         setState(() {
//           currentResult = result;
//           if (currentResult?.text != null) {
//             qrCodeDartScanController.setScanEnabled(false);
//             _startShowDialog(
//               callbackOk: () {
//                 KickBoardStartRequest kickBoardStartRequest =
//                     KickBoardStartRequest(widget.posX, widget.posY);
//                 _setStart(
//                     int.parse(currentResult.toString()), kickBoardStartRequest);
//               },
//               callbackCancel: () {
//                 qrCodeDartScanController.setScanEnabled(true);
//                 Navigator.pop(context);
//               },
//               title: "킥보드를 시작하시겠습니까?",
//             );
//           }
//         });
//       },
//       child: Align(
//         alignment: Alignment.center,
//         child: Container(
//           margin: EdgeInsets.all(20),
//           padding: EdgeInsets.all(20),
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.center,
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Container(
//                   width: 250,
//                   height: 250,
//                   decoration: BoxDecoration(
//                       border: Border.all(width: 3, color: Colors.white),
//                       color: Colors.transparent,
//                       borderRadius: BorderRadius.circular(20))),
//               ComponentMarginVertical(),
//               Container(
//                 alignment: Alignment.center,
//                 width: 200,
//                 height: 30,
//                 decoration: BoxDecoration(
//                     color: Colors.white,
//                     borderRadius: BorderRadius.circular(20)),
//                 child: Text('킥보드 상단 QR코드를 찍으세요'),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//
//   _startShowDialog(
//       {required String title,
//       required VoidCallback callbackOk,
//       required VoidCallback callbackCancel,
//       String? content}) {
//     showDialog(
//         context: context,
//         //barrierDismissible - Dialog를 제외한 다른 화면 터치 x
//         barrierDismissible: false,
//         builder: (BuildContext context) {
//           return AlertDialog(
//             // RoundedRectangleBorder - Dialog 화면 모서리 둥글게 조절
//             shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(10.0)),
//             //Dialog Main Title
//             title: Text(title,
//                 style: TextStyle(
//                     fontSize: fontSizeBig, fontWeight: FontWeight.bold)),
//             actions: <Widget>[
//               TextButton(
//                 child: Text("시작"),
//                 onPressed: callbackOk,
//               ),
//               TextButton(
//                 child: Text("취소"),
//                 onPressed: callbackCancel,
//               ),
//             ],
//           );
//         });
//   }
// }
