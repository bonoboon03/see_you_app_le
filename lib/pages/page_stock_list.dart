import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:see_you_app/components/common/component_appbar_popup.dart';
import 'package:see_you_app/components/common/component_count_title.dart';
import 'package:see_you_app/components/common/component_custom_loading.dart';
import 'package:see_you_app/components/common/component_list_textline_item.dart';
import 'package:see_you_app/components/common/component_no_contents.dart';
import 'package:see_you_app/model/stock_item.dart';
import 'package:see_you_app/pages/page_stock_lack_list.dart';
import 'package:see_you_app/pages/page_stock_list_detail.dart';
import 'package:see_you_app/repository/repo_stock.dart';

class PageStockList extends StatefulWidget {
  const PageStockList({super.key});

  @override
  State<PageStockList> createState() => _PageStockListState();
}

class _PageStockListState extends State<PageStockList> {
  final _scrollController = ScrollController();

  List<StockItem> _list = [];
  int _totalItemCount = 0;
  int _totalPage = 1;
  int _currentPage = 1;

  Future<void> _loadItems({bool reFresh = false}) async {
    // 새로고침하면 초기화시키기.
    if (reFresh) {
      _list = [];
      _totalItemCount = 0;
      _totalPage = 1;
      _currentPage = 1;
    }

    if (_currentPage <= _totalPage) {
      BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
        return ComponentCustomLoading(
          cancelFunc: cancelFunc,
        );
      });

      await RepoStock()
          .getList(page: _currentPage)
          .then((res) => {
                BotToast.closeAllLoading(),
                setState(() {
                  _totalItemCount = res.totalItemCount;
                  _totalPage = res.totalPage;
                  _list = [
                    ..._list,
                    ...?res.list
                  ]; // 기존 리스트에 api에서 받아온 리스트 데이터를 더하는거.
                  _currentPage++;
                })
              })
          .catchError((err) => {
                BotToast.closeAllLoading(),
              });
    }

    // 새로고침하면 스크롤위치를 맨 위로 올리기.
    if (reFresh) {
      _scrollController.animateTo(
        0,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });
    _loadItems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: '재고 리스트'),
      body: ListView(
        controller: _scrollController,
        children: [
          _buildBody(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.update),
          onPressed: () {
            _loadItems(reFresh: true);
          }),
    );
  }

  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Container(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ComponentCountTitle(
                    Icons.attachment, _totalItemCount, '개', '재고 수량'),
                ElevatedButton(
                    child: const Text("재고 부족"),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  PageStockLackList()));
                    }),
              ],
            ),
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (_, index) => ComponentListTextLineItem(
                title: _list[index].productName,
                isUseContent1Line: true,
                content1Subject: '재고 수량',
                content1Text: _list[index].stockQuantity.toString(),
                isUseContent2Line: true,
                content2Subject: '최소 수량',
                content2Text: _list[index].minQuantity.toString(),
                voidCallback: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              PageStockListDetail(
                                  id: _list[index].id,
                                  title: _list[index].productName)));
                },
              ),
            ),
          ],
        ),
      );
    } else {
      // 이용내역이 없을때 흰화면 보이면 에러난것처럼 보이니까 이용내역 없다고 보여주기.
      return SizedBox(
        height: MediaQuery.of(context).size.height - 30 - 50 - 50 - 70,
        child: const ComponentNoContents(
          icon: Icons.history,
          msg: '재고 정보가 없습니다.',
        ),
      );
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
