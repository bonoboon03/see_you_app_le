import 'package:flutter/material.dart';
import 'package:see_you_app/components/common/component_admin_main_button.dart';
import 'package:see_you_app/components/common/component_appbar_actions.dart';
import 'package:see_you_app/components/common/component_margin_horizon.dart';
import 'package:see_you_app/components/common/component_margin_vertical.dart';
import 'package:see_you_app/config/config_color.dart';
import 'package:see_you_app/config/config_size.dart';
import 'package:see_you_app/config/config_style.dart';
import 'package:see_you_app/enums/enum_size.dart';
import 'package:see_you_app/functions/token_lib.dart';
import 'package:see_you_app/pages/page_attendance.dart';
import 'package:see_you_app/pages/page_board_list.dart';
import 'package:see_you_app/pages/page_money_history_list.dart';
import 'package:see_you_app/pages/page_password_update.dart';
import 'package:see_you_app/pages/page_stock_list.dart';

class PageMain extends StatefulWidget {
  const PageMain({super.key});

  @override
  State<PageMain> createState() => _PageMainState();
}

class _PageMainState extends State<PageMain> {
  _ShowDialog({
    required String titleText,
    required Color titleColor,
    required String contentText,
    required Color contentColor,
    required VoidCallback callbackOk,
    required VoidCallback callbackCancel,
  }) {
    showDialog(
        context: context,
        //barrierDismissible - Dialog를 제외한 다른 화면 터치 x
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            // RoundedRectangleBorder - Dialog 화면 모서리 둥글게 조절
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            //Dialog Main Title
            title: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(Icons.report_problem_outlined, color: titleColor),
                ComponentMarginHorizon(enumSize: EnumSize.mid),
                Text(titleText,
                    style: TextStyle(
                        color: titleColor,
                        fontSize: fontSizeBig,
                        fontWeight: FontWeight.bold)),
              ],
            ),
            content: Text(contentText,
                style: TextStyle(
                    color: contentColor,
                    fontSize: fontSizeMid,
                    fontWeight: FontWeight.bold)),
            actions: [
              TextButton(
                child: Text("확인"),
                onPressed: callbackOk,
              ),
              TextButton(
                child: Text("취소"),
                onPressed: callbackCancel,
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(title: "관리자"),
      body: ListView(
        children: [
          const ComponentMarginVertical(),
          Container(
            padding: bodyPaddingLeftRight,
            child: Column(
              children: [
                const ComponentMarginVertical(enumSize: EnumSize.big),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ComponentAdminMainButton(
                      buttonText: "출퇴근 관리",
                      voidCallback: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                const PageBoardList()));
                      },
                    ),
                    ComponentAdminMainButton(
                      buttonText: "로그아웃",
                      voidCallback: () {
                        _ShowDialog(
                          titleText: "로그아웃",
                          titleColor: colorRed,
                          contentText: "정말 로그아웃을 하시겠습니까?",
                          contentColor: colorDarkGray,
                          callbackOk: () {
                            TokenLib.logout(context);
                          },
                          callbackCancel: () {
                            Navigator.pop(context);
                          },
                        );
                      },
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ComponentAdminMainButton(
                      buttonText: "공지 게시판",
                      voidCallback: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    const PageBoardList()));
                        //  PageKickBoardUpdate()));
                      },
                    ),
                    ComponentAdminMainButton(
                      buttonText: "급여 지급 내역",
                      voidCallback: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    const PageMoneyHistoryList()));
                      },
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ComponentAdminMainButton(
                      buttonText: "비밀번호 변경",
                      voidCallback: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                const PagePasswordUpdate()));

                      },
                    ),
                    ComponentAdminMainButton(
                      buttonText: "재고 관리",
                      voidCallback: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    const PageStockList()));

                      },
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}