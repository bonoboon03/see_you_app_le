import 'package:flutter/material.dart';
import 'package:see_you_app/components/common/component_appbar_actions.dart';
import 'package:see_you_app/components/common/component_margin_vertical.dart';
import 'package:see_you_app/config/config_color.dart';
import 'package:see_you_app/config/config_size.dart';
import 'package:see_you_app/config/config_style.dart';
import 'package:see_you_app/enums/enum_size.dart';
import 'package:see_you_app/model/board_list_detail_item_result.dart';
import 'package:see_you_app/repository/repo_board.dart';

class PageBoardListDetail extends StatefulWidget {
  const PageBoardListDetail({super.key, required this.id});

  final int id;

  @override
  State<PageBoardListDetail> createState() => _PageBoardListDetailState();
}

class _PageBoardListDetailState extends State<PageBoardListDetail> {
  String title = "";
  String content = "";
  String dateWrite = "";

  Future<void> _loadBoardDetailData() async {
    BoardListDetailItemResult result =
      await RepoBoard().getBoardDetail(widget.id);

    setState(() {
      title = result.data.title;
      content = result.data.content;
      dateWrite = result.data.dateWrite;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadBoardDetailData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: "리스트 상세보기",
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        Container(
          padding: const EdgeInsets.all(20),
          child: Container(
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: colorLightGray),
              borderRadius: BorderRadius.circular(buttonRadius),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "제목",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      title,
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "작성시간",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      dateWrite,
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                const Text(
                  "내용",
                  style: TextStyle(
                    fontSize: fontSizeMid,
                  ),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.micro),
                Text(
                  content,
                  style: const TextStyle(
                    fontSize: fontSizeMid,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}