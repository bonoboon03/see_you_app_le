import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:see_you_app/components/common/component_appbar_popup.dart';
import 'package:see_you_app/components/common/component_count_title.dart';
import 'package:see_you_app/components/common/component_custom_loading.dart';
import 'package:see_you_app/components/common/component_no_contents.dart';
import 'package:see_you_app/components/component_list_textline_item.dart';
import 'package:see_you_app/model/board_list_item.dart';
import 'package:see_you_app/pages/page_board_list_detail.dart';
import 'package:see_you_app/repository/repo_board.dart';

class PageBoardList extends StatefulWidget {
  const PageBoardList({super.key});


  @override
  State<PageBoardList> createState() => _PageBoardList();
}

class _PageBoardList extends State<PageBoardList> {
  
  final _scrollController = ScrollController();

  List<BoardListItem> _list = [];
  int _totalItemCount = 0;
  int _totalPage = 1;
  int _currentPage = 1;

  Future<void> _loadItems({bool reFresh = false}) async {
    // 새로고침하면 초기화시키기.
    if (reFresh) {
      _list = [];
      _totalItemCount = 0;
      _totalPage = 1;
      _currentPage = 1;
    }

    if (_currentPage <= _totalPage) {
      BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
        return ComponentCustomLoading(
          cancelFunc: cancelFunc,
        );
      });

      await RepoBoard()
          .getBoardList(page: _currentPage)
          .then((res) => {
        BotToast.closeAllLoading(),
        setState(() {
          _totalItemCount = res.totalItemCount;
          _totalPage = res.totalPage;
          _list = [
            ..._list,
            ...?res.list
          ]; // 기존 리스트에 api에서 받아온 리스트 데이터를 더하는거.
          _currentPage++;
        })
      })
          .catchError((err) => {
        BotToast.closeAllLoading(),
        print(err),
      });
    }

    // 새로고침하면 스크롤위치를 맨 위로 올리기.
    if (reFresh) {
      _scrollController.animateTo(
        0,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });
    _loadItems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: '게시판 리스트'),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _loadItems(reFresh: true);
        },
        child: const Icon(Icons.refresh),
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          _buildBody(),
        ],
      ),
    );
  }

  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ComponentCountTitle(Icons.attachment, _totalItemCount, '건', '게시글'),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentListTextLineItem(
              title: _list[index].title,
              isUseContent1Line: true,
              content1Subject: '작성일',
              content1Text: _list[index].dateWrite,
              isUseContent2Line: true,
              content2Subject: '게시글 번호',
              content2Text: '${_list[index].id}',
              voidCallback: () {
                Navigator.push(context, MaterialPageRoute(
                    builder: (BuildContext context) => PageBoardListDetail(id: _list[index].id)));
              },
            ),
          )
        ],
      );
    } else {
      // 이용내역이 없을때 흰화면 보이면 에러난것처럼 보이니까 이용내역 없다고 보여주기.
      return SizedBox(
        height: MediaQuery.of(context).size.height - 30 - 50 - 50 - 70,
        child: const ComponentNoContents(
          icon: Icons.history,
          msg: '게시글이 없습니다.',
        ),
      );
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
