import 'package:dio/dio.dart';
import 'package:see_you_app/config/config_api.dart';
import 'package:see_you_app/functions/token_lib.dart';
import 'package:see_you_app/model/board_list_detail_item_result.dart';
import 'package:see_you_app/model/board_list_item_result.dart';

class RepoBoard {
 // Future -> 만약 서버가 느리면 올때까지 기다려야함 미래에 줄거야. / async -> 비동기. 일단 진행시켜라
  Future<BoardListItemResult> getBoardList({int page = 1}) async {
    // 여기서 주소는 변하지 않는다. 그래서 const를 넣는다.
    const String _baseUrl = 'http://localhost:8087/v1/board/all/{page}';

    // print(_baseUrl.replaceAll('{page}', page.toString()));

    String? token = await TokenLib.getMemberToken();

    // API 호출할때 쓰는 전화기
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';
    // final 변하지않는다는 의미 / await 기다려라.
    final response = await dio.get(
      _baseUrl.replaceAll("{page}", page.toString()),

      options: Options(
        // followRedirects -> 간혹 성공,실패하면 여기로 가시오 하는 경우가 있다. 따라게 할 지 여부
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      ),
    );
    return BoardListItemResult.fromJson(response.data);
  }

  Future<BoardListDetailItemResult> getBoardDetail(int id) async {
    const String baseUrl = 'http://localhost:8087/v1/board/board?id={id}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        baseUrl.replaceAll("{id}", id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));
    return BoardListDetailItemResult.fromJson(response.data);
  }
}