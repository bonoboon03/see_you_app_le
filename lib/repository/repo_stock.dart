import 'package:dio/dio.dart';
import 'package:see_you_app/functions/token_lib.dart';
import 'package:see_you_app/model/common_result.dart';
import 'package:see_you_app/model/stock_list_result.dart';
import 'package:see_you_app/model/stock_request.dart';
import 'package:see_you_app/model/stock_result.dart';

class RepoStock {
  Future<CommonResult> putStock(int id, StockRequest request) async {
    const String baseUrl = 'http://localhost:8083/v1/stock/update/{id}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        baseUrl.replaceAll('{id}', id.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<StockListResult> getList({int page = 1}) async {
    const String baseUrl = 'http://localhost:8083/v1/stock/all/{page}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        baseUrl.replaceAll('{page}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return StockListResult.fromJson(response.data);
  }

  Future<StockResult> getStock(int id) async {
    const String baseUrl = 'http://localhost:8083/v1/stock/stock?id={id}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return StockResult.fromJson(response.data);
  }

  Future<StockListResult> getLackList() async {
    const String baseUrl = 'http://localhost:8083/v1/stock/lack-list';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return StockListResult.fromJson(response.data);
  }
}