import 'package:dio/dio.dart';
import 'package:see_you_app/config/config_api.dart';
import 'package:see_you_app/functions/token_lib.dart';
import 'package:see_you_app/model/common_result.dart';
import 'package:see_you_app/model/login_request.dart';
import 'package:see_you_app/model/login_result.dart';
import 'package:see_you_app/model/update_password_request.dart';

class RepoMember {
  Future<LoginResult> doLogin(String memberGroup, LoginRequest loginRequest) async {
    const String baseUrl = 'http://localhost:8081/v1/member/login/app/{memberGroup}';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl.replaceAll('{memberGroup}', memberGroup),
        data: loginRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return LoginResult.fromJson(response.data);
  }

  Future<CommonResult> updatePassword(UpdatePasswordRequest updatePasswordRequest) async {
    const String baseUrl = 'http://localhost:8081/v1/member/password';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        baseUrl,
        data: updatePasswordRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }
}